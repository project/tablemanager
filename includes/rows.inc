<?php

/**
 * Display table row adding/ editing forms.
 */
function tablemanager_manage_row($form_state = array(), $op, $param) {
  switch ($op) {
    case 'add':
      // Set the title, but check if the name is simply a number and prefix it with "Table xx"
      drupal_set_title($param == tablemanager_fetch_name($param) ? t("Table $param") : tablemanager_fetch_name($param));
      return tablemanager_manage_row_form($form_state, $op, $param);
      break;
    case 'edit':
      drupal_set_title(t("Edit Table '$param->name' Entry"));
      $data = unserialize($param->data);
      $form_state['values'] = tablemanager_process_row_data($data);
      return tablemanager_manage_row_form($form_state, $op, $param->tid);
      break;
  }
  return $form;
} // tablemanager_manage_row

/**
 * Form for table row adding/ editing.
 */
function tablemanager_manage_row_form(&$form_state, $op, $tid, $row = NULL) {
  $count = 1;
  $fetch = db_fetch_object(db_query('SELECT * FROM {tablemanager} WHERE tid = %d', $tid));
  foreach (unserialize($fetch->header) as $a) {
    $types = tablemanager_fetch_types();
    $type = is_array($a) && array_key_exists('type', $a) ? $a['type'] : '1';
    $string = $types[$type];
    $required = is_array($a) && array_key_exists('required', $a) ? $a['required'] : FALSE;
    if ($type == "6") {
      $string = sprintf($string, $a['low'], $a['high']);
      $form["low_$count"] = array(
        '#type' => 'hidden',
        '#value' => $a['low'],
      );
      $form["high_$count"] = array(
        '#type' => 'hidden',
        '#value' => $a['high'],
      );
    }
    if ($type == "7") {
      $choices = explode("\n", $a['choices']);
    }
    $a = is_array($a) && array_key_exists('data', $a) && !$a['data'] ? t("Column $count") : is_array($a) && array_key_exists('data', $a) ? $a['data'] : $a;
    if ($type == "3") {
      unset($date);
      if (isset($form_state['values']["item_$count"])) {
        $temp = explode('/', $form_state['values']["item_$count"]);
        $date['day'] = $temp['2'] ? $temp['2'] : date('d');
        $date['month'] = $temp['1'] ? $temp['1'] : date('m');
        $date['year'] = $temp['0'] ? $temp['0'] : date('Y');
      }
      $form["item_$count"] = array(
        '#type' => 'date',
        '#title' => $a,
        '#default_value' => $date,
        '#description' => $string,
        '#required' => $required,
      );
    }
    elseif ($type == "7") {
      $form["item_$count"] = array(
        '#type' => 'select',
        '#title' => $a,
        '#options' => $choices,
        '#default_value' => isset($form_state['values']["item_$count"]) ? $form_state['values']["item_$count"] : '0',
        '#description' => $string,
        '#required' => $required,
      );
    }
    else {
      $form["item_$count"] = array(
        '#type' => 'textfield',
        '#title' => $a,
        '#default_value' => isset($form_state['values']["item_$count"]) ? $form_state['values']["item_$count"] : '',
        '#size' => 60,
        '#maxlength' => 255,
        '#description' => $a ? t("Enter your data for '%col' here<br />%type", array('%col' => $a, '%type' => $string)) :  t('Enter your data for column %col here<br />%type', array('%col' => $count, '%type' => $string)),
        '#attributes' => NULL,
        '#required' => $required,
      );
    }
    $form["name_$count"] = array(
      '#type' => 'hidden',
      '#value' => $a,
    );
    $form["type_$count"] = array(
      '#type' => 'hidden',
      '#value' => $type,
    );
    $form["required_$count"] = array(
      '#type' => 'hidden',
      '#value' => $required,
    );
    $count++;
  } // end foreach
  $form['format'] = filter_form($form['format']);
  $form['cols'] = array(
    '#type' => 'hidden',
    '#value' => $count - 1,
  );
  $form['tid'] = array(
    '#type' => 'hidden',
    '#value' => $tid,
  );
  $form['row'] = array(
    '#type' => 'hidden',
    '#value' => arg(2),
  );
  $form['action'] = array(
    '#type' => 'hidden',
    '#value' => $op,
  );
  $form['backpage'] = array(
    '#type' => 'hidden',
    '#value' => isset($form_state['post']['backpage']) ? $form_state['post']['backpage'] : referer_uri(),
  );
  return confirm_form($form, NULL, $form_state['post']['backpage'] ? $form_state['post']['backpage'] : $form['backpage']['#value'], ' ', t('Submit'), t('Cancel'));
} // tablemanager_manage_row_form

/**
 * Validation function for tablemanager_edit_form
 */
function tablemanager_manage_row_validate($form, &$form_state) {
  for ($i = 1; $i <= $form_state['values']['cols']; $i++) {
    switch ($form_state['values']["type_$i"]) {
      case "2": // Numeric
        if ($form_state['values']["item_$i"] && !is_numeric($form_state['values']["item_$i"])) {
          form_set_error("item_$i", t('%name field must be numeric.', array('%name' => $form_state['values']["name_$i"])));
        }
        break;
      case "3": // Date
        if ($form_state['values']["item_$i"]) {
//        date_validate($form_state['values']["item_$i"]);  // removed as dates are auto-validated
        }
        break;
      case "4": // URL
        if ($form_state['values']["item_$i"]) {
          if (!valid_url($form_state['values']["item_$i"])) {
            form_set_error("item_$i", t('Please enter a valid URL.'));
            break;
          }
          else {
            $form_state['values']["item_$i"] = check_url($form_state['values']["item_$i"]);
          }
        }
        break;
      case "5": // Email
        if ($form_state['values']["item_$i"] && !valid_email_address($form_state['values']["item_$i"])) {
          form_set_error("item_$i", t('Please enter a valid email address.'));
        }
        break;
      case "6": // Bar Chart
        if ($form_state['values']["item_$i"]) {
          if (!is_numeric($form_state['values']["item_$i"])) {
            form_set_error("item_$i", t('%name field must be numeric.', array('%name' => $form_state['values']["name_$i"])));
          }
          if ($form_state['values']["item_$i"] < $form_state['values']["low_$i"] || $form_state['values']["item_$i"] > $form_state['values']["high_$i"]) {
            form_set_error("item_$i", t('%name field must contain a number between %low and %high.', array('%name' => $form_state['values']["name_$i"], '%low' => $form_state['values']["low_$i"], '%high' => $form_state['values']["high_$i"])));
          }
        }
        break;
    } // end switch
  } // end for loop
} // tablemanager_manage_row_validate

/**
 * Submit function for tablemanager_edit_form
 */
function tablemanager_manage_row_submit($form, &$form_state) {
  global $user;
  $row = array();
  for ($i = 1; $i <= $form_state['values']['cols']; $i++) {
    if ($form_state['values']["type_$i"] == '3') {
      $temp = $form_state['values']["item_$i"]['year'] .'/'. $form_state['values']["item_$i"]['month'] .'/'. $form_state['values']["item_$i"]['day'];
    }
    elseif ($form_state['values']["type_$i"] == '1') {
      $temp = trim(check_markup($form_state['values']["item_$i"], $form_state['values']['format']));
    }
    else {
      $temp = $form_state['values']["item_$i"];
    }
    if (substr($temp, 0, 3) == "<p>" && substr($temp, -4, 4) == "</p>") {
      $temp = substr($temp, 3, -4);
    }
    if ($temp == "<br />") {
      $temp = "";
    }
    array_push($row, $temp);
  } // end for loop
  $row = serialize($row);
  switch ($form_state['values']['action']) {
    case 'add':
      // insert into database
      db_query("INSERT INTO {tablemanager_data} (tid, uid, data, format) VALUES (%d, %d, '%s', %d)", $form_state['values']['tid'], $user->uid, $row, $form_state['values']['format']);
      drupal_set_message(t('Row has been added to table %table.', array('%table' => $form_state['values']['tid'])));
      watchdog('tablemanager', 'tablemanager: added row to table \'%table\'.', array('%table' => $form_state['values']['tid']), WATCHDOG_NOTICE, l('view', 'tables/'. $form_state['values']['tid']));
      break;
    case 'edit':
      // update database
      db_query("UPDATE {tablemanager_data} SET data = '%s', format = %d WHERE id = %d", $row, $form_state['values']['format'], $form_state['values']['row']);
      drupal_set_message(t('Row has been edited.'));
      watchdog('tablemanager', 'tablemanager: updated row in table \'%tid\'.', array('%tid' => $form_state['values']['tid']), WATCHDOG_NOTICE, l('view', 'tables/'. $form_state['values']['tid']));
      break;
  }
  $form_state['redirect'] = $form_state['values']['backpage'];
} // tablemanager_manage_row_submit

/**
 * Form for row delete function.
 */
function tablemanager_row_delete($form_state = array(), $info) {
  $form['tid'] = array(
    '#type' => 'hidden',
    '#value' => $info['tid'],
  );
  $form['rows'] = array(
    '#type' => 'hidden',
    '#value' => implode(",", $info['rows']),
  );
  $form['backpage'] = array(
    '#type' => 'hidden',
    '#value' => isset($form_state['post']['backpage']) ? $form_state['post']['backpage'] : referer_uri(),
  );
  $form['row'] = array(
    '#type' => 'item',
    '#value' => tablemanager_display_rows($info),
  );
  return confirm_form($form, t('Delete Table \'%table\' Entr!plural1:<p>Are you sure you want to delete row!plural2 %title?', array('!plural1' => count($info['rows']) > 1 ? 'ies': 'y', '!plural2' => count($info['rows']) > 1 ? 's': '', '%title' => implode(", ", $info['rows']), '%table' => $info['tid'])), $form_state['post']['backpage'] ? $form_state['post']['backpage'] : $form['backpage']['#value'], t('This action cannot be undone.'), t('Delete'), t('Cancel'));
} // tablemanager_row_delete

/**
 * Submit function for tablemanager_row_delete form.
 */
function tablemanager_row_delete_submit($form, &$form_state) {
  $count = 0;
  foreach (explode(",", $form_state['values']['rows']) as $row) {
    db_query('DELETE FROM {tablemanager_data} WHERE id = %d', $row);
    $count++;
  }
  drupal_set_message(t('The row!plural been deleted.', array('!plural' => $count > 1 ? 's have' : ' has')));
  watchdog('tablemanager', "tablemanager: deleted row!plural in table '%tid'.", array('!plural' => $count > 1 ? 's' : '', '%tid' => $form_state['values']['tid']), WATCHDOG_NOTICE, l('view', 'tables/'. $form_state['values']['tid']));
  $form_state['redirect'] = $form_state['values']['backpage'];
} // tablemanager_row_delete_submit

/**
 * Processes the row data into a form_state compatible array.
 */
function tablemanager_process_row_data($data) {
  $count = 1;
  foreach ($data as $cell) {
    $values["item_$count"] = $cell;
    $count++;
  }
  return $values;
} // tablemanager_process_row_data
