<?php

/**
 * Display settings for Tablemanager tables.
 */
function tablemanager_display_settings() {
  $form['display_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Settings:'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['display_settings']['tablemanager_names'] = array(
    '#type' => 'select',
    '#title' => t('Display Names'),
    '#default_value' => variable_get('tablemanager_names', NULL),
    '#options' => array(NULL => 'Display Nothing', 'names' => 'Show Names', 'table number' => 'Table Numbers'),
    '#description' => t('What to show in the main hook ie. <code>&#91;tablemanager&#58;x&#93;</code>'),
  );
  $form['display_settings']['tablemanager_descriptions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Table Descriptions?'),
    '#default_value' => variable_get('tablemanager_descriptions', 0),
    '#description' => t('Whether to display the table description as a caption in the main hook'),
  );
  $form['display_settings']['tablemanager_date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date Format'),
    '#default_value' => variable_get('tablemanager_date_format', 'jS F Y'),
    '#size' => 10,
    '#description' => t('A full list of codes can be found !here', array('!here' => l('here', 'http://www.php.net/manual/en/function.date.php'))),
  );
  $form['display_settings']['tablemanager_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use tablemanager.css?'),
    '#default_value' => variable_get('tablemanager_css', 0),
    '#description' => t('Whether to use tablemanager.css on your tables'),
  );
  return system_settings_form($form);
} // tablemanager_display_settings

/**
 * Bar Chart settings for Tablemanager tables.
 */
function tablemanager_bar_chart_settings() {
  $bars = array();
  $result = file_scan_directory(drupal_get_path('module', 'tablemanager') .'/misc', 'bar.png$');
  if (!$result) {
    variable_set('tablemanager_bc_colour', 'disable');
    drupal_set_message(t('Bar chart colours are either missing or not readable by the server'), 'error');
    return;
  }
  else {
    foreach ($result as $obj) {
      $temp = substr($obj->name, 0, -4);
      $bars[$temp] = ucfirst($temp);
    }
  }
  $form['display_settings']['barchart'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bar Chart Display Settings:'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['display_settings']['barchart']['tablemanager_bc_colour'] = array(
    '#type' => 'select',
    '#title' => t('Set the colour for your barchart'),
    '#default_value' => variable_get('tablemanager_bc_colour', 'red'),
    '#options' => $bars,
    '#description' => t('Which colour bars to display bar charts in'),
  );
  $form['display_settings']['barchart']['tablemanager_bc_end'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display bar value?'),
    '#default_value' => variable_get('tablemanager_bc_end', 1),
    '#description' => t('Whether to display the stored database value at the end of your bar charts'),
  );
  return system_settings_form($form);
} // tablemanager_bar_chart_settings

/**
 * Instructions for Tablemanager usage.
 */
function tablemanager_instructions() {
  $form['tablemanager_instructions'] = array(
    '#type' => 'markup',
    '#value' => t("<p>A simple module which automates the creation of tables and allows you and sufficiently permissioned users to add to them and maintain them. The tables can be displayed in any node simply by enabling the 'Tablemanager' input filter and typing:</p>
        <p><code>&#91;tablemanager&#58;1&#93;</code></p>

        <p>The best thing about it is that the tables it creates are Drupal themed tables and so easily fit in with the look and feel of your site.  It's also possible to use this module to display nice looking themed bar charts.</p>
        <p>Tablemanager supports pagination (splitting the results into pages - <strong>NOTE</strong> the default is to display ALL the results). For instance, to display table 2 limited to 25 results per page you'd type:</p>
        <p><code>&#91;tablemanager&#58;2, 25&#93;</code></p>

        <p>If you write your own php snippets you can display your tables from within your code:</p>
        <p><div class='codeblock'><code><font color='#000000'><font color='#0000BB'>&lt;?php<br /></font><font color='#007700'>...<br /></font><font color='#0000BB'>&#36;output </font><font color='#007700'>.= </font><font color='#DD0000'>&quot;This is table '4':&lt;p&gt;&quot;</font><font color='#007700'>;<br /></font><font color='#0000BB'>&#36;output </font><font color='#007700'>.= </font><font color='#0000BB'>tablemanager_display</font><font color='#007700'>(</font><font color='#0000BB'>4</font><font color='#007700'>, </font><font color='#0000BB'>20</font><font color='#007700'>);<br /></font><font color='#0000BB'>&#36;output </font><font color='#007700'>.= </font><font color='#DD0000'>&quot;&lt;/p&gt;&quot;</font><font color='#007700'>;<br />return </font><font color='#0000BB'>&#36;output</font><font color='#007700'>;<br /></font><font color='#0000BB'>?&gt;</font></font></code></div></p>

        <p>If you are providing your users with certain administrative privileges you can also display links to allow them to add new rows and edit/ delete their own entries.  This is set by either a TRUE (edit and delete links ON) or FALSE (links OFF which is the default).</p>
        <p><code>&#91;tablemanager&#58;1, 35, TRUE&#93;</code></p>

        <p>If you wish to restrict the rows displayed you can do so by passing arguments to the filter like this:</p>
        <p><code>&#91;tablemanager&#58;1, NULL, FALSE, column &#61; 2 &#124; start &#61; &quot;a&quot; &#124; end &#61; &quot;h&quot;&#93;</code></p>

        <p>If you wish to only show rows which match a particular string you can omit the 'end' key:</p>
        <p><code>&#91;tablemanager&#58;1, NULL, FALSE, column &#61; 1 &#124; start &#61; &quot;New 2006&quot;&#93;</code></p>

        <p>For an advanced example; how to show everyone in a 'date of birth' table who is under 25 years of age using the php input filter:</p>
        <p><div class='codeblock'><code><font color='#000000'><font color='#0000BB'>&lt;?php<br /></font><font color='#007700'>print </font><font color='#0000BB'>tablemanager_display</font><font color='#007700'>(</font><font color='#0000BB'>1</font><font color='#007700'>, </font><font color='#0000BB'>NULL</font><font color='#007700'>, </font><font color='#0000BB'>FALSE</font><font color='#007700'>, array(</font><font color='#DD0000'>'column' </font><font color='#007700'>=&gt; </font><font color='#0000BB'>3</font><font color='#007700'>, </font><font color='#DD0000'>'start' </font><font color='#007700'>=&gt; </font><font color='#0000BB'>date</font><font color='#007700'>(</font><font color='#DD0000'>'Y/m/d'</font><font color='#007700'>, </font><font color='#0000BB'>mktime</font><font color='#007700'>(</font><font color='#0000BB'>0</font><font color='#007700'>, </font><font color='#0000BB'>0</font><font color='#007700'>, </font><font color='#0000BB'>0</font><font color='#007700'>, </font><font color='#0000BB'>date</font><font color='#007700'>(</font><font color='#DD0000'>'m'</font><font color='#007700'>), </font><font color='#0000BB'>date</font><font color='#007700'>(</font><font color='#DD0000'>'d'</font><font color='#007700'>), </font><font color='#0000BB'>date</font><font color='#007700'>(</font><font color='#DD0000'>'Y'</font><font color='#007700'>)-</font><font color='#0000BB'>25</font><font color='#007700'>)), </font><font color='#DD0000'>'end' </font><font color='#007700'>=&gt; </font><font color='#0000BB'>date</font><font color='#007700'>(</font><font color='#DD0000'>'Y/m/d'</font><font color='#007700'>)));<br /></font><font color='#0000BB'>?&gt;</font></font></code></div></p>

        <p>You can also pass parameters to the display function for the &lt;table&gt; tag, such as setting borders (and border rules), frame rules, cellspacing/ cellpadding and alignment. Experiment with this code on one of your own tables:</p>
        <p><code>&#91;tablemanager&#58;1, NULL, FALSE, NULL, title &#61; My Table &#124; bgcolor &#61; yellow &#124; border &#61; 5 &#124; frame &#61; vsides&#93;</code></p>

        <p>For more information please visit this page:</p>
        <p>!link</p>
        <p>Whilst its easy enough to create tables manually, this module allows you to avoid writing any code at all (so great for non-programmers) it inserts your tables into the database so any changes you make to a table will reflect on whatever pages you have the table displayed on - which can be however many pages you like.  You can create as many tables as you like and manage all of them in the admin screen.</p>
        <p>It's now also possible to automatically create bar charts with this module.  When you create a new table you're given the option to create it as a bar chart.  There are several settings you can apply as well, such as the high and low values acceptable to each bar chart you create, plus the colour of the bars and theming/ display options.</p>
        <p>You can edit all of your tables however you like;</p>
        <ul>
        <li>edit the header, table name and description</li>
        <li>allow end user to sort only on specified columns (or have no sorting at all)</li>
        <li>set a column to be default for sorting when table is first displayed (also set default for which direction to sort in)</li>
        <li>add more rows</li>
        <li>edit all your data</li>
        <li>delete rows</li>
        <li>add and remove columns</li>
        </ul>
        <p>In !menu you can enable a menu item which will list all tables when a permissioned user/ administrator goes to:</p>
        <p>!url</p>
        <p>The user can then click on the links to be taken to the table they wish to view.  If you're permissioning users to be able to create their own tables, this is where they'll appear - you should also enable 'Display Administrative Links' in !settings under 'Table List Screen Settings' so these users can then manage the tables they have created.</p>",
        array('!url' => l(url('tablemanager', array('absolute' => TRUE)), 'tablemanager'), '!settings' => l('admin/settings/tablemanager', 'admin/settings/tablemanager'), '!menu' => l('admin/build/menu', 'admin/build/menu'), '!link' => l('http://www.w3schools.com/tags/tag_table.asp', 'http://www.w3schools.com/tags/tag_table.asp'))),
  );
  return $form;
} // tablemanager_instructions
