<?php

/**
 * Main multipart form function.
 */
function tablemanager_table_add($form_state = array()) {
  switch ($form_state['storage']['op']) {
    default:
      return tablemanager_table_add_step_one();
      break;
    case t('Next'):
      return tablemanager_table_add_step_two($form_state);
      break;
    case t('Create'):
      return tablemanager_table_add_step_three($form_state['storage']);
      break;
  }
} // tablemanager_table_add

/**
 * Combined validate function for multipart form.
 */
function tablemanager_table_add_validate($form, &$form_state) {
  switch ($form_state['clicked_button']['#post']['op']) {
    case t('Next'):
      tablemanager_validatestring($form_state['values']['name'], 'name');
      break;
    case t('Create'):
      for ($i = 1; $i <= $form_state['values']['cols']; $i++) {
        if (is_numeric($form_state['values']['default']) && !$form_state['values']['item_'. $form_state['values']['default']]) {
          form_set_error('item_'. $form_state['values']['default'], t('Due to Drupal restrictions, a sortable column cannot have a blank id.'));
        }
        $form_state['values']["item_$i"] = check_plain($form_state['values']["item_$i"]);
        // Bar Chart validation:
        if ($form_state['values']["type_$i"] == "6") {
          if (!$form_state['values']["low_$i"] || !is_numeric($form_state['values']["low_$i"])) {
            if ($form_state['values']["low_$i"] == '0') {
            }
            else {
              form_set_error("low_$i", t('Low field is required for bar chart and must be numeric'));
            }
          }
          if (!$form_state['values']["high_$i"] || !is_numeric($form_state['values']["high_$i"])) {
            if ($form_state['values']["high_$i"] == '0') {
            }
            else {
              form_set_error("high_$i", t('High field is required for bar chart and must be numeric'));
            }
          }
          if ($form_state['values']["low_$i"] >= $form_state['values']["high_$i"]) {
            form_set_error('high', t('High field cannot be lower or equal to low field'));
          }
        }
        // End Bar Chart validation
      } // end for loop
      if (is_numeric($form_state['values']['default']) && !$form_state['values']['sort_'. $form_state['values']['default']]) {
        form_set_error('sort_'. $form_state['values']['default'], t('Default sort column set to column %col - which isn\'t set to be sortable?', array('%col' => $form_state['values']['default'])));
      }
      break;
  }
} // tablemanager_table_add_validate

/**
 * Combined submit function for multipart form.
 */
function tablemanager_table_add_submit($form, &$form_state) {
  // Save previous form values for next step
  $form_state['storage'] = $form_state['values'];
} // tablemanager_table_add_submit

/**
 * First step of multipart form.
 */
function tablemanager_table_add_step_one() {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Table Name',
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('You can leave these fields blank and your tables will automatically be named: 1, 2, 3, 4...'),
  );
  $form['desc'] = array(
    '#type' => 'textfield',
    '#title' => 'Table Description',
    '#size' => 90,
    '#maxlength' => 255,
    '#description' => t('Optional - Table description will be shown to users when they add new rows.'),
  );
  $form['cols'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of columns'),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => 'Enter the number of columns you wish to have in your table.',
    '#required' => TRUE,
  );
  $form['backpage'] = array(
    '#type' => 'hidden',
    '#value' => isset($form_state['post']['backpage']) ? $form_state['post']['backpage'] : referer_uri(),
  );
  return confirm_form($form, NULL, $form_state['post']['backpage'] ? $form_state['post']['backpage'] : $form['backpage']['#value'], ' ', t('Next'), t('Cancel'));
} // tablemanager_table_add_step_one

/**
 * Second step of multipart form.
 */
function tablemanager_table_add_step_two(&$form_state) {
  $form_state['rebuild'] = TRUE;
  $types = array(1 => t('Text'), 2 => t('Numeric'), 3 => t('Date'), 4 => t('URL'), 5 => t('Email'), 6 => t('Bar Chart'), 7 => t('Selection'));
  $form['order'] = array(
    '#type' => 'select',
    '#title' => t('Which order to sort for default?'),
    '#options' => array('desc' => 'desc', 'asc' => 'asc'),
    '#description' => t('In which order the default column to be sorted will be in.'),
    '#default_value' => isset($form_state['values']['order']) ? $form_state['values']['order'] : 'desc',
  );
  $form['default'] = array(
    '#type' => 'radio',
    '#title' => t('No default column for sorting'),
    '#return_value' => "NULL",
    '#default_value' => isset($form_state['values']['default']) ? $form_state['values']['default'] : TRUE,
  );
  for ($i = 1; $i <= $form_state['storage']['cols']; $i++) {
    $form["col_$i"] = array(
      '#type' => 'fieldset',
      '#title' => t('Column %col:', array('%col' => $i)),
      '#collapsible' => TRUE,
      '#collapsed' => $i == 1 ? FALSE : TRUE,
    );
    $form["col_$i"]["item_$i"] = array(
      '#type' => 'textfield',
      '#title' => NULL,
      '#size' => 60,
      '#maxlength' => 255,
      '#default_value' => isset($form_state['values']["item_$i"]) ? $form_state['values']["item_$i"] : '',
      '#description' => t('Enter your data for the HEADER column %col here.', array('%col' => $i)),
    );
    $form["col_$i"]["type_$i"] = array(
      '#type' => 'select',
      '#title' => t('Data type'),
      '#options' => $types,
      '#default_value' => isset($form_state['values']["type_$i"]) ? $form_state['values']["type_$i"] : '',
    );
    $form["col_$i"]["required_$i"] = array(
      '#type' => 'checkbox',
      '#title' => t('Required?'),
      '#description' => t('Check if column requires an entry'),
      '#default_value' => isset($form_state['values']["required_$i"]) ? $form_state['values']["required_$i"] : FALSE,
    );
    $form["col_$i"]["sorting_$i"] = array(
      '#type' => 'fieldset',
      '#title' => t('Sorting Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form["col_$i"]["sorting_$i"]["sort_$i"] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow sorting on column %col?', array('%col' => $i)),
      '#default_value' => isset($form_state['values']["sort_$i"]) ? $form_state['values']["sort_$i"] : FALSE,
    );
    $form["col_$i"]["sorting_$i"]['default'] = array(
      '#type' => 'radio',
      '#title' => 'Default column to sort on?',
      '#return_value' => $i,
      '#default_value' => isset($form_state['values']['default']) ? $form_state['values']['default'] : FALSE,
    );
    $form["col_$i"]["bc_$i"] = array(
      '#type' => 'fieldset',
      '#title' => t('Bar Chart Options'),
      '#description' => t('Please note that you only have to fill this out if you select \'Bar Chart\' as your data type'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form["col_$i"]["bc_$i"]["low_$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Enter lowest value enterable for bar charts'),
      '#size' => 4,
      '#maxlength' => 4,
      '#default_value' => isset($form_state['values']["low_$i"]) ? $form_state['values']["low_$i"] : '0',
      '#required' => TRUE,
    );
    $form["col_$i"]["bc_$i"]["high_$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Enter highest value enterable for bar charts'),
      '#size' => 4,
      '#maxlength' => 4,
      '#default_value' => isset($form_state['values']["high_$i"]) ? $form_state['values']["high_$i"] : '500',
      '#required' => TRUE,
    );
    $form["col_$i"]["select_$i"] = array(
      '#type' => 'fieldset',
      '#title' => t('Selection Box Choices'),
      '#description' => t('Please note that you only have to fill this out if you select \'Selection\' as your data type'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form["col_$i"]["select_$i"]["sb_$i"] = array(
      '#type' => 'textarea',
      '#title' => t('Enter your selections below, with each choice separated by a newline'),
      '#default_value' => isset($form_state['values']["sb_$i"]) ? $form_state['values']["sb_$i"] : '',
      '#cols' => 60,
      '#rows' => 25,
    );
  } // end for loop
  $form['desc'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($form_state['storage']['desc']),
  );
  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($form_state['storage']['name']),
  );
  $form['cols'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($form_state['storage']['cols']),
  );
  $form['backpage'] = array(
    '#type' => 'hidden',
    '#value' => $form_state['clicked_button']['#post']['backpage'],
  );
  return confirm_form($form, NULL, $form_state['clicked_button']['#post']['backpage'], ' ', t('Create'), t('Cancel'));
} // tablemanager_table_add_step_two

/**
 * Last step of multipart form (creates the entry in the db).
 */
function tablemanager_table_add_step_three($storage) {
  $name = $storage['name'];
  $desc = $storage['desc'];
  $row = array();
  for ($i = 1; $i <= $storage['cols']; $i++) {
    $temp = array('data' => $storage["item_$i"], 'type' => $storage["type_$i"], 'required' => $storage["required_$i"]);
    if ($i == $storage['default']) {
      $temp['field'] = "$i";
      $temp['sort'] = $storage['order'];
    }
    elseif ($storage["sort_$i"]) {
      $temp['field'] = "$i";
    }
    if ($storage["type_$i"] == "6") {
      $temp['low'] = $storage["low_$i"];
      $temp['high'] = $storage["high_$i"];
    }
    if ($storage["type_$i"] == "7") {
      $temp['choices'] = $storage["sb_$i"];
    }
    array_push($row, $temp);
  } // end for loop
  $row = serialize($row);
  global $user;
  // insert into database
  db_query("INSERT INTO {tablemanager} (uid, name, description, header) VALUES (%d, '%s', '%s', '%s')", $user->uid, $name, $desc, $row);
  $sql = db_fetch_object(db_query("SELECT tid FROM {tablemanager} ORDER BY tid DESC"));
  variable_set('tablemanager_table', $sql->tid);
  // if tablename is blank, set it to be same as table id
  if (!$name) {
    $name = $sql->tid;
    db_query("UPDATE {tablemanager} SET name = '%s' WHERE tid = %d", $name, $name);
  }
  drupal_set_message(t('Table has been created.'));
  watchdog('tablemanager', 'tablemanager: added table \'%table\'.', array('%table' => $sql->tid), WATCHDOG_NOTICE, l('view', "tables/$sql->tid"));
  menu_rebuild();
  drupal_goto($storage['backpage']);
} // tablemanager_table_add_step_three

/**
 * Form for table delete function.
 */
function tablemanager_table_delete($form_state = array(), $tid) {
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($tid),
  );
  $form['table'] = array(
    '#type' => 'item',
    '#value' => tablemanager_display_rows(array('tid' => $tid)),
  );
  return confirm_form($form, t('Are you sure you want to delete table \'!title\'?', array('!title' => theme('placeholder', tablemanager_fetch_name($tid)))), 'admin/content/tablemanager', t('This action cannot be undone and will destroy ALL table data.'), t('Delete'), t('Cancel'));
} // tablemanager_table_delete

/**
 * Submit function for tablemanager_table_delete form.
 */
function tablemanager_table_delete_submit($form, &$form_state) {
  db_query('DELETE FROM {tablemanager} WHERE tid = %d', $form_state['values']['id']);
  db_query('DELETE FROM {tablemanager_data} WHERE tid = %d', $form_state['values']['id']);
  drupal_set_message(t('Table %table has been deleted.', array('%table' => $form_state['values']['id'])));
  watchdog('tablemanager', 'tablemanager: deleted table \'%table\'.', array('%table' => $form_state['values']['id']), WATCHDOG_NOTICE);
  if ($form_state['values']['id'] == variable_get('tablemanager_table', '1')) {
    tablemanager_set_tid(variable_get('tablemanager_set_tid', 'DESC'));
  }
  menu_rebuild();
  $form_state['redirect'] = 'admin/content/tablemanager';
} // tablemanager_table_delete_submit

/**
 * Form for table edit function.
 */
function tablemanager_table_edit($form_state = array(), $tid) {
  drupal_add_css(drupal_get_path('module', 'tablemanager') .'/misc/tablemanager_admin.css');
  if (isset($form_state['storage']) && element_children($form_state['storage']['column'])) {
    foreach (element_children($form_state['storage']['column']) as $storage) {
      $headers[] = $form_state['storage']['column'][$storage];
    }
  }
  elseif (isset($form_state['storage'])) {
    // this'll never happen, just here for testing
  }
  else {
    // this is the first time this form has been called so build it from scratch
    $table = db_fetch_object(db_query('SELECT * FROM {tablemanager} WHERE tid = %d', $tid));
    $headers = unserialize($table->header);
  }
  $form['tid'] = array(
    '#type' => 'hidden',
    '#value' => $tid,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Table Name'),
    '#default_value' => isset($form_state['storage']['name']) ? $form_state['storage']['name'] : $table->name,
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Edit Table name here.'),
  );
  $form['desc'] = array(
    '#type' => 'textfield',
    '#title' => t('Table Description'),
    '#default_value' => isset($form_state['storage']['desc']) ? $form_state['storage']['desc'] : $table->description,
    '#size' => 90,
    '#maxlength' => 255,
    '#description' => t('Edit Table description here.'),
  );
  // set some variables for building the rows
  $form['column']['#tree'] = TRUE;
  unset($sort);
  $direction = 'asc'; // set 'asc' for default direction for sorting
  // build rows
  foreach ($headers as $position => $header) {
    if (preg_match("/^(asc|desc)/i", $header['sort'], $match)) {
      $sort = $position; // there's a match for default sort column, so set $sort to show which column it's on
      $direction = $match[0]; // direction is set so change the default to reflect this
    }
    $form['column'][$position]['weight'] = array('#type' => 'weight', '#value' => $position);
    $form['column'][$position]['data'] = array('#type' => 'textfield', '#default_value' => $header['data'], '#size' => 60, '#maxlength' => 255);
    $form['column'][$position]['type'] = array('#type' => 'select', '#options' => tablemanager_fetch_types(FALSE), '#default_value' => $header['type'], '#attributes' => array('DISABLED' => 'TRUE'));
    $form['column'][$position]['required'] = array('#type' => 'checkbox', '#return_value' => TRUE, '#default_value' => isset($header['required']) ? $header['required'] : FALSE);
    $form['column'][$position]['field'] = array('#type' => 'checkbox', '#return_value' => TRUE, '#default_value' => isset($header['field']) ? $header['field'] : FALSE);
    $form[$position]['default'] = array('#type' => 'radio', '#return_value' => $position, '#value' => $position === $sort ? "$position" : FALSE);
    $form['column'][$position]['delete'] = array('#type' => 'submit', '#name' => "delete_$position", '#value' => t('Delete'));
  }
  $form['order'] = array(
    '#type' => 'select',
    '#title' => t('Which order to sort for default?'),
    '#options' => array('desc' => 'desc', 'asc' => 'asc'),
    '#description' => t('In which order the default column to be sorted will be in.'),
    '#default_value' => $direction,
  );
  $form['default'] = array(
    '#type' => 'radio',
    '#title' => t('No default column for sorting'),
    '#return_value' => "NULL",
    '#value' => is_null($sort) ? TRUE : FALSE, // sort wasn't set so reflect that here
  );

  $form['description'] = array('#value' => t('Changes are not permanent until after you click save.'));
  $form['actions'] = array('#prefix' => '<div class="container-inline">', '#suffix' => '</div>');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['actions']['cancel'] = array('#value' => l(t('Cancel'), 'admin/content/tablemanager'));

  return $form;
} // tablemanager_table_edit

function tablemanager_table_edit_submit($form, &$form_state) {
  $form_state['storage'] = $form_state['values'];
  // organise default sort radio button
//  if ($form_state['clicked_button']['#post']['default'] >= 0) {
    print_r($form_state['clicked_button']['#post']);
//  }
  if (preg_match("/^delete_(\d+)$/i", $form_state['clicked_button']['#name'], $match)) {
    unset($form_state['storage']['column'][$match[1]]);
    // check to see if all columns are to be deleted
    if (count(element_children($form_state['storage']['column'])) == 0) {
      // redirect to table delete
      drupal_goto('admin/content/tablemanager/table_delete/table-'. $form_state['values']['tid']);
    }
  }

} // tablemanager_table_edit_submit

function theme_tablemanager_table_edit($form) {
  drupal_add_tabledrag('tablemanager-table-edit', 'order', 'self', 'header-weight');
  $header = array(t('Column Title'), t('Entry Type'), t('Required'), t('Sortable'), t('Default<br/>(sorting)'), t('Weight'), t('Operations'));
  $output = drupal_render($form['name']);
  $output .= drupal_render($form['desc']);
  $output .= drupal_render($form['order']);
  $output .= drupal_render($form['default']);
  foreach (element_children($form['column']) as $position) {
    unset($cols);
    $form['column'][$position]['weight']['#attributes']['class'] = 'header-weight';
    $cols[] = drupal_render($form['column'][$position]['data']);
    $cols[] = drupal_render($form['column'][$position]['type']);
    $cols[] = drupal_render($form['column'][$position]['required']);
    $cols[] = drupal_render($form['column'][$position]['field']);
    $cols[] = drupal_render($form[$position]['default']);
    $cols[] = drupal_render($form['column'][$position]['weight']);
    $cols[] = drupal_render($form['column'][$position]['delete']);
    $rows[$position] = array('data' => $cols, 'class' => 'draggable');
  }
  $output .= theme('table', $header, $rows, array('id' => 'tablemanager-table-edit'));
  // Render remaining fields
  $output .= drupal_render($form);
  return $output;
} // theme_tablemanager_table_edit
