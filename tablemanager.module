<?php

/**
 * @file
 * This module allows sufficiently permissioned users to create and maintain
 * Drupal generated themeable tables.
 *
 */

/**
 * Core Drupal hooks:
 */

/**
 * Implementation of hook_access().
 */
function tablemanager_access($op, $node, $account) {
  switch ($node) {
    case 'tablemanager':
      foreach (tablemanager_tables() as $no => $table) {
        if ($op == 'create') {
          if (user_access('administer tables') || user_access('create '. $table['name'] .' content')) {
            return TRUE;
          }
          elseif (user_access('administer/ create own tables') && tablemanager_nodeadd()) {
            return TRUE;
          }
        }
      }
      break;
    case 'table':
      if ($op == 'create') {
        if (user_access('administer tables') || user_access('administer/ create own tables')) {
          return TRUE;
        }
      }
      break;
  }
} // tablemanager_access

/**
 * Implementation of hook_filter().
 */
function tablemanager_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return (array(0 => t('Tablemanager filter')));
      break;
    case 'name':
      return t('tablemanager filter');
      break;
    case 'description':
      return t('Substitutes [tablemanager: ...] tags with the corresponding table');
      break;
    case 'no cache':
      return TRUE;
      break;
    case 'prepare':
      return $text;
      break;
    case 'process':
      return _tablemanager_process_text($text);
      break;
  } // end switch
} // tablemanager_filter

/**
 * Implementation of hook_filter_tips().
 */
function tablemanager_filter_tips($delta, $format, $long = FALSE) {
  if ($long) {
    return t('You can embed tablemanager tables within your nodes using the following syntax:<br/>
             [tablemanager:table_id,pagination,admin_links,column=?|start=?|end=?,attribute=?|attribute=?|...]<br />
             All arguments bar table_id are optional:
             <ul>
             <li><strong>table_id</strong> = table number</li>
             <li><strong>pagination</strong> = list length (numeric or NULL for all results)</li>
             <li><strong>admin_links</strong> = TRUE or FALSE to enable/ disable administrative links</li>
             <li><strong>column=?|start=?|end=?</strong> = can be in any order:<ul><li><strong>column</strong> = column to search on (numeric)</li><li><strong>start</strong> = match from</li><li><strong>end</strong> = match end (optional)</li></ul></li>
             <li><strong>attributes</strong> = add as many attributes as you like separated by \'|\', for example border=2|bgcolor=yellow</li>
             </ul>');
  }
  else {
    return t('You can embed tablemanager tables within your nodes using the following syntax:<br/>[tablemanager:table_id,pagination,admin_links,column=?|start=?|end=?,attribute=?|attribute=?|...]');
  }
} // tablemanager_filter_tips

/**
 * Implementation of hook_help().
 */
function tablemanager_help($path, $arg) {
  switch ($path) {
    case 'node/add/tablemanager': // Shown when you click through 'table entry' on the create content screen
      return t('Choose from the following available tables:');
    case 'node/add/tablemanager/%': // Shown when you add a new row
      return t("<p>!desc</p>Add a new entry to table %table. Fill in your data and click submit.",
        array(
          '%table' => tablemanager_table_load($arg[3]),
          '!desc' => tablemanager_desc(tablemanager_table_load($arg[3])),
        )
      );
  }
} // tablemanager_help

/**
 * Implementation of hook_menu().
 */
function tablemanager_menu() {
  $items = array();
  // main configuration page of the basic tablemanager module
  $items['admin/settings/tablemanager'] = array(
    'title' => 'Tablemanager Settings',
    'description' => 'Administer Tablemanagers tables/ users.',
    'file' => 'includes/settings.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_instructions'),
    'access arguments' => array('administer tablemanager settings'),
    'type' => MENU_NORMAL_ITEM,
  );
  // the default local task (needed when other modules want to offer
  // alternative tablemanager types and their own configuration page as local task)
  $items['admin/settings/tablemanager/settings'] = array(
    'title' => 'Instructions',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -20,
  );
  $items['admin/settings/tablemanager/display'] = array(
    'title' => 'Display',
    'description' => 'Display settings for the administration screen.',
    'file' => 'includes/settings.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_display_settings'),
    'access arguments' => array('administer tablemanager settings'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/settings/tablemanager/bar_chart'] = array(
    'title' => 'Bar Chart',
    'description' => 'Display settings for the Bar Chart entry type.',
    'file' => 'includes/settings.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_bar_chart_settings'),
    'access arguments' => array('administer tablemanager settings'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  // Table delete + edit links
  $items['admin/content/tablemanager/table_delete/%tablemanager_table'] = array(
    'file' => 'includes/tables.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_table_delete', 4),
    'access arguments' => array('administer tables'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/content/tablemanager/table_edit/%tablemanager_table'] = array(
    'file' => 'includes/tables.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_table_edit', 4),
    'access arguments' => array('administer tables'),
    'type' => MENU_CALLBACK,
  );
  // Row delete + edit links
  $items['tablemanager/edit/%tablemanager_row'] = array(
    'file' => 'includes/rows.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_manage_row', 'edit', 2),
    'access callback' => '_tablemanager_manage_row_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );
  $items['tablemanager/%tablemanager_function'] = array(
    'file' => 'includes/rows.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_row_delete', 1),
    'load arguments' => array('%map', '%index'),
    'access callback' => '_tablemanager_manage_row_access',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
  );
  return $items;
} // tablemanager_menu

/**
 * Implementation of hook_menu_alter().
 */
function tablemanager_menu_alter(&$callbacks) {
  // Overwrite the default callbacks for node/add page
  $callbacks['node/add/table'] = array(
    'title' => 'Table',
    'description' => 'Create a new table.',
    'title callback' => 'check_plain',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_table_add'),
    'access callback' => '_tablemanager_table_add_access',
    'module' => 'tablemanager',
    'file' => 'includes/tables.inc',
  );
  $callbacks['node/add/tablemanager']['page callback'] = 'drupal_get_form';
  $callbacks['node/add/tablemanager']['page arguments'] = array('tablemanager_nodeadd_form');
  $callbacks['node/add/tablemanager']['access callback'] = '_tablemanager_table_add_access';
  $callbacks['node/add/tablemanager']['module'] = 'tablemanager';
  unset($callbacks['node/add/tablemanager']['file']);
  $callbacks['node/add/tablemanager/%tablemanager_table']['page callback'] = 'drupal_get_form';
  $callbacks['node/add/tablemanager/%tablemanager_table']['page arguments'] = array('tablemanager_manage_row', 'add', 3);
  $callbacks['node/add/tablemanager/%tablemanager_table']['access callback'] = '_tablemanager_table_add_access';
  $callbacks['node/add/tablemanager/%tablemanager_table']['module'] = 'tablemanager';
  $callbacks['node/add/tablemanager/%tablemanager_table']['file'] = 'includes/rows.inc';
} // tablemanager_menu_alter

/**
 * Implementation of hook_node_info().
 */
function tablemanager_node_info() {
  $tables = tablemanager_nodeadd();
  if ($tables) $description .= t('<dd>Choose from the following available tables: !tables', array('!tables' => tablemanager_nodeadd()));
  return array('table' => array('name' => t('table'), 'module' => 'tablemanager', 'description' => t('Create a new table.')),
    'tablemanager' => array('name' => t('table entry'), 'module' => 'tablemanager', 'description' => $description)
  );
} // tablemanager_node_info

/**
 * Implementation of hook_perm().
 */
function tablemanager_perm() {
  $perms = array('administer tables', 'list tables', 'administer/ create own tables');
  foreach (tablemanager_tables() as $table) {
    $perms[] = t('create @table content', array('@table' => $table['name']));
    $perms[] = t('edit any @table content', array('@table' => $table['name']));
    $perms[] = t('edit own @table content', array('@table' => $table['name']));
  }
  return $perms;
} // tablemanager_perm

/**
 * Implementation of hook_theme().
 */
function tablemanager_theme() {
  return array(
    'tablemanager_table_edit' => array(
      'arguments' => array('form'),
      'file' => 'includes/tables.inc',
    ),
  );
} // tablemanager_theme

/**
 * Menu access callbacks:
 */

/**
 * Row add/ delete/ edit form access.
 */
function _tablemanager_manage_row_access($info) {
// broken, but broken with a reason...  I need to alter this to cope with multiple selections with both delete AND edit
  global $user;
  unset($flag);
  $flag = $user->uid == $info->uid && user_access("edit own $info->name content") ? TRUE : $flag;
  $flag = user_access('administer tables') || user_access("edit any $info->name content") ? TRUE : $flag;
  $flag = $user->uid == $info->tableuid && user_access('administer/ create own tables') ? TRUE : $flag;
  return $flag ? TRUE : FALSE;
} // _tablemanager_manage_row_access

/**
 * Table add form access.
 */
function _tablemanager_table_add_access() {
  return user_access('administer tables') || user_access('administer/ create own tables') ? TRUE : FALSE;
} // _tablemanager_table_add_access

/**
 * Table add form access.
 */
function _tablemanager_table_manage_access() {
  return user_access("create ". $table['name'] ." content") || user_access('administer tables') || user_access('administer/ create own tables') && $user->uid == $table['uid'] ? TRUE : FALSE;
} // _tablemanager_table_manage_access

/**
 * Menu load arguments:
 */

/**
 * Row functions.
 */
function tablemanager_function_load($op, $rows, $index) {
  // remove 'tablemanager/(delete|edit)'
  $rows = array_slice($rows, $index + 1);
  unset($tid);
  switch ($op) {
    case 'delete':
    case 'edit':
      // 'rows' can't be empty
      if (empty($rows)) return FALSE;
      foreach ($rows as $row) {
        // check all rows are numeric
        if (!is_numeric($row)) {
          return FALSE;
        }
        // check all rows exist and have same parent table
        $fetch = db_result(db_query('SELECT tid FROM {tablemanager_data} WHERE id = %d', $row));
        if (!$fetch) {
          return FALSE;
        }
        if (!$tid) {
          $tid = $fetch;
          continue;
        }
        if ($tid != $fetch) {
          return FALSE;
        }
      }
      // if tests pass, return something useful
      return array('tid' => $tid, 'rows' => $rows);
      break;
  }
  return FALSE;
} // tablemanager_function_load

/**
 * Ensures the table-xxx parameter passed through node/add/tablemanager + admin/content/tablemanager/table_delete/xxx is valid.
 */
function tablemanager_table_load($string) {
  $check = preg_match('/^table-(\d+)/i', $string, $match);
  if (!$check) {
    return FALSE;
  }
  if (tablemanager_table_exists($match[1])) {
    return $match[1];
  }
  return FALSE;
} // tablemanager_table_load

/**
 * Tablemanager functions:
 */

/**
 * Returns attributes if they're present.
 */
function tablemanager_attributes(&$text, &$attributes) {
  $pattern = "/\[(align|axis|bgcolor|class|style|valign) *= *(\w+)\]/i";
  preg_match_all($pattern, $text, $matches);
  foreach ($matches[0] as $no => $match) {
    $attributes[] = array($matches[1][$no] => $matches[2][$no]);
    $text = str_replace($matches[0][$no], '', $text);
  }
} // tablemanager_attributes

/**
 * Imports css file.
 */
function tablemanager_css($tid = 0) {
  // quick check that we've been passed a number
  if (!is_numeric($tid)) {
    return;
  }
  // check if a css file exists for given tid
  $path = "/tablemanager_css/tablemanager_0.css";
print file_check_path($path);
  if ($file = file_check_path($path)) {
    drupal_add_css($file);
  }
  else {
    // else import the default css file
    drupal_add_css(drupal_get_path('module', 'tablemanager') .'/misc/tablemanager0.css');
  }
} // tablemanager_css

/**
 * Returns description field from specified table.
 */
function tablemanager_desc($tid) {
  $fetch = db_fetch_object(db_query('SELECT description FROM {tablemanager} WHERE tid = %d', $tid));
  return $fetch->description;
} // tablemanager_desc

/**
 * The main user hook to display a table.
 */
function tablemanager_display_new($tid, $list_length = NULL, $params = array()) {
  global $user, $pager_page_array, $pager_total_items, $pager_total;
  // check parameters are valid
  if (!$tid || !is_numeric($tid)) {
    return 'Invalid Table ID';
  }
  if (isset($params['display'])) {
    $params['display']['end'] = $params['display']['end'] ? $params['display']['end'] : $params['display']['start'];
  }
  elseif (!is_array($params['display'])) {
    unset($params['display']);
  }
  if (!is_array($params['attributes'])) {
    unset($params['attributes']);
  }
  $params['links'] = $params['links'] === "FALSE" ? FALSE : $params['links'];
  $uid = tablemanager_fetch_uid($tid);
  if ($list_length == NULL || $list_length == "NULL") {
    $list_length = db_result(db_query('SELECT COUNT(id) FROM {tablemanager_data} WHERE tid = %d', $tid));
    if (!$list_length) {
      $list_length = 1;
    }
  }

  // to-do...

} //tablemanager_display

/**
 * The main user hook to display a table.
 */
function tablemanager_display($tid, $list_length = NULL, $links = FALSE, $date = array(), $attrib = array()) {
  global $user, $pager_page_array, $pager_total_items, $pager_total;
  if (!$tid || !is_numeric($tid)) {
    return 'Invalid Table ID';
  }
  if ($date) {
    $date['end'] = $date['end'] ? $date['end'] : $date['start'];
  }
  elseif (!is_array($date)) {
    $date = array();
  }
  if (!is_array($attrib)) {
    $attrib = array();
  }
  $links = $links === "FALSE" ? FALSE : $links;
  $uid = tablemanager_fetch_uid($tid);
  if ($list_length == NULL || $list_length == "NULL") {
    $list_length = db_result(db_query('SELECT COUNT(id) FROM {tablemanager_data} WHERE tid = %d', $tid));
    if (!$list_length) {
      $list_length = 1;
    }
  }
  $fetch = db_query('SELECT tmd.id, tm.uid AS tableuid, tmd.uid, tm.name, tm.description, tm.header, tmd.data FROM {tablemanager} tm LEFT JOIN {tablemanager_data} tmd ON tm.tid = tmd.tid WHERE tm.tid = %d ORDER BY tmd.id', $tid);
  $path = base_path() . drupal_get_path('module', 'tablemanager');
  unset($flag);
  $rows = array();
  $types = array();
  while ($result = db_fetch_object($fetch)) {
    if (!$header) {
      $header = unserialize($result->header);
      foreach ($header as $a) {
        if (is_array($a) && array_key_exists('type', $a)) {
          array_push($types, $a['type']);
        }
        else {
          array_push($types, '1');
        }
      }
      if (variable_get('tablemanager_css', 0)) {
        tablemanager_css();
      }
      $description = variable_get('tablemanager_descriptions', 0) ? $result->description : NULL;
      switch (variable_get('tablemanager_names', NULL)) {
        case "table number":
          $name = "Table ". $tid;
          break;
        case "names":
          $name = $result->name == $tid ? "Table ". $tid : $result->name;
          break;
        default:
          $name = NULL;
          break;
      } // end switch
      if ($links) {
        if ($user->uid == $result->tableuid && user_access('administer/ create own tables') || user_access('edit own '. tablemanager_fetch_name($tid) .' content') || user_access('edit any '. tablemanager_fetch_name($tid) .' content') || user_access('administer tables')) {
          if (in_array($user->uid, $uid) || user_access('edit any '. tablemanager_fetch_name($tid) .' content') || user_access('administer tables') || user_access('administer/ create own tables')) {
            array_push($header, array('data' => t('Operations'), 'colspan' => '2'));
            $flag = TRUE;
          }
        }
      }
    }
    $data = unserialize($result->data);
    unset($cancelrow);
    if ($data) {
      // Check for data types
      unset($temp);
      $col = 0;
      foreach ($data as $a) {
        $attributes = array();
        if ($a == "||") { // Colspan
          $last = array_pop($temp);
          $up1 = $last['data'];
          $up2 = $last['colspan'] ? $last['colspan'] : '1';
          $up2++;
          $temp[$col + 1] = array('data' => $up1, 'colspan' => $up2);
          $col++;
          continue;
        }
        if ($a || $a == "0") {
          if ($date && $col == $date['column']-1) {
            if (strcasecmp($a, $date['start']) < 0 || strcasecmp($a, $date['end']) > 0) {
              $cancelrow = TRUE;
              break;
            }
          }
          $temp[$col + 1] = array('data' => tablemanager_process_cell($col, $a, $header[$col]));
        }
        else {
          $temp[$col + 1] = array('data' => '');
        }
        $col++;
      } // end foreach
    } // end if
    if ($cancelrow) {
      unset($temp);
    }
    if ($temp && $links && $flag) {
      if ($user->uid == $result->uid || user_access("edit any ". tablemanager_fetch_name($tid) ." content") || user_access('administer tables') || user_access('administer/ create own tables')) {
        array_push($temp, l(t('edit'), "tablemanager/edit/$result->id"), l(t('delete'), "tablemanager/delete/$result->id"));
      }
      else {
        array_push($temp, NULL, NULL);
      }
    }
    if ($temp) {
      $rows[] = array('data' => $temp);
    }
  } // end while
  if (!$header) {
    return 'Invalid Table ID';
  }
  if (!$rows) {
    $output[] = array(array('data' => t('No table data available.'), 'colspan' => 2));
    if ($flag) {
      array_pop($header);
    }
  }
  $temp = tablesort_get_order($header);
  if ($temp['sql']) {
    tablemanager_sort($rows, $temp['sql'], tablesort_get_sort($header));
  }
  // if results need to be paged we have to split $row (which contains ALL the results)
  // into separate pages because we're having to avoid using 'pager' functionality
  $count = 1;
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  $pager_page_array = explode(',', $page);
  $from = ($pager_page_array[$tid] * $list_length);
  foreach ($rows as $temp) {
    if ($count > $from && $count <= ($from + $list_length)) {
      $output[] = $temp;
    }
    $count++;
  } // end foreach
  if (!array_key_exists('class', $attrib) && variable_get('tablemanager_css', 0)) {
    $attrib['class'] = 'tablemanager';
    $attrib['id'] = 'tablemanager-table-'. $tid;
  }
  if (variable_get('tablemanager_names', NULL)) $table .= "<h2>$name</h2>";
  $table .= theme('table', $header, $output, $attrib, $description);
  if ($links) {
    $table .= ($user->uid == $result->tableuid && user_access('administer/ create own tables')) || user_access('create '. tablemanager_fetch_name($tid) .' content') || user_access('administer tables') ? '<p>'. l(t('Add new entry'), "node/add/tablemanager/table-$tid") : '';
  }
  // set global pager variables to correct values:
  $pager_total_items[$tid] = count($rows);
  $pager_total[$tid] = ceil($pager_total_items[$tid]/$list_length);
  $pager_page_array[$tid] = max(0, min((int)$pager_page_array[$tid], ((int)$pager_total[$tid]) - 1));
  $table .= theme('pager', array(), $list_length, $tid);
  return $table;
} // tablemanager_display

/**
 * Returns supplied rows in table form.
 */
function tablemanager_display_rows($params = array()) {
  if (!is_array($params)) {
    return FALSE;
  }
  $full_header = unserialize(db_result(db_query('SELECT header FROM {tablemanager} WHERE tid = %d', $params['tid'])));
  $fetch = db_query('SELECT id, uid, data FROM {tablemanager_data} WHERE tid = %d ORDER BY id', $params['tid']);
  if (!isset($params['rows'])) {
    // select a random row from table for an 'example' row
    $temp = db_result(db_query('SELECT id FROM {tablemanager_data} WHERE tid = %d ORDER BY RAND()', $params['tid']));
    // only append to rows array if a result is returned
    if ($temp) {
      $params['rows'][] = $temp;
    }
  }
  if (!empty($params['rows'])) {
    foreach ($params['rows'] as $row_id) {
      $row = unserialize(db_result(db_query('SELECT data FROM {tablemanager_data} WHERE id = %d', $row_id)));
      foreach ($row as $col => $cell) {
        // Allow for colspan
        if ($cell == "||") {
          if ($col > 0) {
            $last = array_pop($table[$row_id]);
            $up1 = $last['data'];
            $up2 = $last['colspan'] ? $last['colspan'] : '1';
            $up2++;
            $table[$row_id][] = array('data' => $up1, 'colspan' => $up2);
            continue;
          }
          else {
            // colspan is set for first cell so just blank it
            $table[$row_id][$col] = array('data' => '');
            continue;
          }
        }
        $table[$row_id][$col] = array('data' => tablemanager_process_cell($col, $cell, $full_header[$col]));
      }
    }
  }
  // strip out everything but data field as display doesn't require any special viewing
  foreach ($full_header as $info) {
    $header[] = $info['data'];
  }
  return theme('table', $header, $table);
} // tablemanager_display_rows

/**
 * Returns the user entered name for a table.
 */
function tablemanager_fetch_name($tid) {
  $result = db_fetch_object(db_query('SELECT name FROM {tablemanager} WHERE tid = %d', $tid));
  return $result->name;
} // tablemanager_fetch_name

/**
 * Returns an array of valid tablemanager entry types.
 */
function tablemanager_fetch_types($descriptions = TRUE) {
  if ($descriptions) {
    return array(1 => NULL, 2 => t('Please enter a numeric value'), 3 => t('Please enter a valid date'), 4 => t("Please enter a full URL, such as 'http://www.yourlink.com'"), 5 => t('Please enter a valid email address'), 6 => t('Please enter a number between %d and %d'), 7 => t(''));
  }
  return array(1 => t('Text'), 2 => t('Numeric'), 3 => t('Date'), 4 => t('URL'), 5 => t('Email'), 6 => t('Bar Chart'), 7 => t('Selection'));
} // tablemanager_fetch_types

/**
 * Returns an array of users who have entered data in a table.
 */
function tablemanager_fetch_uid($tid) {
  $uid = array();
  $sql = db_query('SELECT uid FROM {tablemanager_data} WHERE tid = %d', $tid);
  while ($result = db_fetch_object($sql)) {
    $uid[] = $result->uid;
  }
  return $uid;
} // tablemanager_fetch_uid

/**
 * Returns a list of table ids.
 */
function tablemanager_load_tables() {
  $tables = array();
  $sql = db_query('SELECT tid, name FROM {tablemanager} ORDER BY tid');
  while ($result = db_fetch_object($sql)) {
    $tables[$result->tid] = $result->tid == $result->name ? "Table $result->tid" : "Table $result->tid: $result->name";
  }
  return $tables;
} // tablemanager_load_tables

/**
 * Returns all table types for create content page.
 */
function tablemanager_nodeadd() {
  global $user;
  unset($flag);
  $items = '<dl>';
  foreach (tablemanager_tables() as $no => $table) {
    if (user_access('administer/ create own tables') && $user->uid == $table['uid'] || user_access('create '. $table['name'] .' content') || user_access('administer tables')) {
      $table['name'] = $table['name'] == $table['tid'] ? 'Table '. $table['name'] : $table['name'];
      $table['description'] = $table['description'] ? $table['description'] : '&nbsp;';
      $items .= '<dt>'. l($table['name'], "node/add/tablemanager/table-$no") .'</dt><dd>'. $table['description'] .'</dd>';
      $flag = TRUE;
    }
  }
  $items .= '</dl>';
  if (!$flag) {
    return FALSE;
  }
  return $items;
} // tablemanager_nodeadd

/**
 * Returns a form displaying table types for create content page.
 */
function tablemanager_nodeadd_form() {
  return array('display' => array('#type' => 'markup', '#value' => tablemanager_nodeadd()));
} // tablemanager_nodeadd_form

/**
 * Function to apply entry types to cells.
 */
function tablemanager_process_cell($col, $data, $args = array()) {
  $attributes = array();
  $path = drupal_get_path('module', 'tablemanager');
  switch ($args['type']) {
    default: // Text + Numeric
      tablemanager_attributes($data, $attributes);
      $temp = $data;
      if ($attributes) {
        foreach ($attributes as $attribute) {
          foreach ($attribute as $key => $value) {
            $temp[$key] = $value;
          }
        }
      }
      break;
    case "3": // Date
      $exp = explode('/', $data);
      $temp = "<!-- $data -->". date(variable_get('tablemanager_date_format', 'jS F Y'), mktime(0, 0, 0, $exp['1'], $exp['2'], $exp['0']));
      break;
    case "4": // URL
      // To do - split this nicely into display name and link rather than one entry for both
      $temp = l($data, $data);
      break;
    case "5": // Email
      // To do - split this nicely into display name and email rather than one entry for both
      $temp = l($data, 'mailto:'. $data);
      break;
    case "6": // Bar Chart
      $temp = theme_image($path .'/misc/'. variable_get('tablemanager_bc_colour', 'red') .'-bar.png', $data, '', array('style' => 'vertical-align: middle; margin: 5px 5px 5px 0;', 'width' => $data, 'height' => '16'), FALSE);
      if (variable_get('tablemanager_bc_end', 1)) {
        $temp = $temp . $data;
      }
      break;
    case "7": // Selection
      $choices = explode("\n", $args['choices']);
      $temp = $choices[$data];
      break;
    case "8": // Node Reference
      break;
    case "9": // User Reference
      break;
  } // end switch
  return $temp;
} // tablemanager_process_cell

/**
 * Function to match and process the tablemanager filter.
 */
function _tablemanager_process_text($text) {
  $pattern = "/\[tablemanager:(\d+,?\w*,?\w*,?\w*=?\d*\|?\w*=?\w*\|?\w*=?\w*,?.*?)\]/i";
  if (preg_match_all($pattern, $text, $matches)) {
    $date = array();
    $attrib = array();
    foreach ($matches[1] as $no => $args) {
      $arg = explode(',', $args);
      if ($arg[3]) {
        $params = explode('|', $arg[3]);
        foreach ($params as $param) {
          $temp = explode('=', $param);
          $date[trim($temp[0])] = trim(trim($temp[1]), '"');
        }
      }
      if ($arg[4]) {
        $params = explode('|', $arg[4]);
        foreach ($params as $param) {
          $temp = explode('=', $param);
          $attrib[trim($temp[0])] = trim($temp[1]);
        }
      }
      $text = str_replace($matches[0][$no], tablemanager_display(trim($arg[0]), trim($arg[1]), trim($arg[2]), $date, $attrib), $text);
    }
  }
  return $text;
} // _tablemanager_process_text

/**
 * Ensures the row parameter passed through tablemanager/xxx is valid.
 */
function tablemanager_row_load($id) {
  if (!is_numeric($id)) {
    return FALSE;
  }
  $fetch = db_fetch_object(db_query('SELECT tm.tid, tm.uid AS tableuid, tm.name, tmd.uid, tm.header, tmd.data, tmd.format
                                     FROM {tablemanager} tm
                                       INNER JOIN {tablemanager_data} tmd ON tm.tid = tmd.tid
                                     WHERE tmd.id = %d', $id));
  if ($fetch) {
    return $fetch;
  }
  else {
    return FALSE;
  }
} // tablemanager_row_load

/**
 * Sets to a valid table if current table is deleted.
 */
function tablemanager_set_tid($dir = 'DESC') {
  $fetch = db_fetch_object(db_query('SELECT tid FROM {tablemanager} ORDER BY tid %s', $dir));
  $tid = $fetch->tid;
  variable_set('tablemanager_table', $tid);
  return;
} // tablemanager_set_tid

/**
 * Sort function.
 */
function tablemanager_sort(&$data, $field, $sort = "asc") {
  $next = $field + 1;
  $code = array_key_exists($next, $data) ? "if (0 == (\$cmp = strnatcasecmp(strip_tags_c(\$a['data']['$field']['data']), strip_tags_c(\$b['data']['$field']['data'])))) return strnatcasecmp(strip_tags_c(\$a['data']['$next']['data']), strip_tags_c(\$b['data']['$next']['data'])); else return \$cmp;" : "return strnatcasecmp(strip_tags_c(\$a['data']['$field']['data']), strip_tags_c(\$b['data']['$field']['data']));";
  if ($sort == "asc") {
    uasort($data, create_function('$a,$b', $code));
  }
  else {
    uasort($data, create_function('$b,$a', $code));
  }
} // tablemanager_sort

/**
 * Checks to see if a table actually exists.
 */
function tablemanager_table_exists($tid) {
  $fetch = db_result(db_query('SELECT tid FROM {tablemanager} WHERE tid = %d', $tid));
  if ($fetch) {
    return TRUE;
  }
  return FALSE;
} // tablemanager_table_exists

/**
 * Returns an array containing all tables.
 */
function tablemanager_tables() {
  $tables = array();
  $sql = db_query('SELECT * FROM {tablemanager}');
  while ($result = db_fetch_object($sql)) {
    $tables[$result->tid] = array('tid' => $result->tid, 'uid' => $result->uid, 'name' => $result->name, 'description' => $result->description, 'header' => $result->header);
  }
  return $tables;
} // tablemanager_tables

/**
 * Validates string.
 */
function tablemanager_validatestring($word, $name) {
  if (eregi('[^a-z 0-9_-]', $word)) {
    form_set_error($name, t('The specified %name contains one or more illegal characters.<br />Special characters except space, dash (-) and underscore (_) are not allowed.', array('%name' => $name)));
  }
  return;
} // tablemanager_validatestring

/**
 * Other functions:
 */

/**
 * Comments aren't sortable, so we temporarily alter them to sort on them.
 */
function strip_tags_c($string, $allowed_tags = '<!-->') {
  $allow_comments = (strpos($allowed_tags, '<!-->') !== false );
  if ($allow_comments) {
    $string = str_replace(array('<!--', '-->'), array('&lt;!--', '--&gt;'), $string);
    $allowed_tags = str_replace('<!-->', '', $allowed_tags);
  }
  $string = strip_tags($string, $allowed_tags);
  if ($allow_comments) {
    $string = str_replace(array('&lt;!--', '--&gt;'), array('<!--', '-->'), $string);
  }
  return $string;
} // strip_tags_c
