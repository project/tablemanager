<?php

/**
 * Implementation of hook_help().
 */
function tablemanager_css_help($path, $arg) {
  switch ($path) {
    case 'admin/content/tablemanager/styling':
      return t('<p>Basic automated css styling.  Extremely experimental, behaviour may be erratic depending on how much your theme takes control of tables.
        <br />Some selections may appear to do nothing when altered, this is likely because they rely on another style selection to be enabled first, eg. if you have a border width of 0px and/ or border style of \'none\' don\'t expect to see any change when you alter the border colour.<p />'
      );
  }
} // tablemanager_css_help

/**
 * Implementation of hook_menu().
 */
function tablemanager_css_menu() {
  $items = array();
  $items['admin/content/tablemanager/styling'] = array(
    'title' => 'Customize CSS',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_css_styling'),
    'access arguments' => array('administer tables'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );
  return $items;
} // tablemanager_css_menu

/**
 * Implementation of hook_theme().
 */
function tablemanager_css_theme() {
  return array(
    'tablemanager_css_styling' => array(
      'arguments' => array('form'),
    ),
  );
} // tablemanager_css_theme

/**
 * Experimental CSS style generator
 */
function tablemanager_css_styling($form_state) {
  $form['styles'] = array(
    '#type' => 'select',
    '#options' => array('None', 'Business', 'Garland', 'Black and White'),
    '#title' => t('Pre-defined Styles'),
    '#default_value' => 0,
    '#attributes' => array('class' => 'table-styles'),
  );
  $form['tabs']['#tree'] = TRUE;
  $form['tabs']['table']['width-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '25px',
    '#name' => 'tabs[table][width-custom]', // set our own name tag as we're using drupal_render
  );
  $form['tabs']['table']['width'] = array(
    '#type' => 'radios',
    '#options' => array('auto' => 'auto', '100%' => '100%', '75%' => '75%', '50%' => '50%', 'custom' => drupal_render($form['tabs']['table']['width-custom'])),
    '#title' => t('width'),
    '#default_value' => '75%',
    '#attributes' => array('class' => 'table-options'),
  );
  $form['tabs']['table']['overflow'] = array(
    '#type' => 'radios',
    '#options' => array('visible' => 'visible', 'hidden' => 'hidden', 'scroll' => 'scroll', 'auto' => 'auto'),
    '#title' => t('overflow'),
    '#default_value' => 'auto',
    '#attributes' => array('class' => 'table-options'),
  );
  $form['tabs']['table']['border-width'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', 'thin' => 'thin', 'medium' => 'medium', 'thick' => 'thick', '0px' => '0px'),
    '#title' => t('border-width'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'table-options'),
  );
  $form['tabs']['table']['border-spacing'] = array(
    '#type' => 'radios',
    '#options' => array('2px' => '2px', '1px' => '1px', '3px' => '3px', '4px' => '4px', '5px' => '5px', '0px' => '0px'),
    '#title' => t('border-spacing'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'table-options'),
  );
  $form['tabs']['table']['border-style'] = array(
    '#type' => 'radios',
    '#options' => array('outset' => 'outset', 'none' => 'none', 'hidden' => 'hidden', 'dotted' => 'dotted', 'dashed' => 'dashed', 'solid' => 'solid', 'double' => 'double', 'ridge' => 'ridge', 'groove' => 'groove', 'inset' => 'inset'),
    '#title' => t('border-style'),
    '#default_value' => 'none',
    '#attributes' => array('class' => 'table-options'),
  );
  $form['tabs']['table']['border-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[table][border-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['table']['border-color'] = array(
    '#type' => 'radios',
    '#options' => array('gray' => 'gray', 'white' => 'white', 'blue' => 'blue', 'green' => 'green', 'black' => 'black', 'red' => 'red', 'custom' => drupal_render($form['tabs']['table']['border-color-custom'])),
    '#title' => t('border-color'),
    '#default_value' => 'white',
    '#attributes' => array('class' => 'table-options'),
  );
  $form['tabs']['table']['border-collapse'] = array(
    '#type' => 'radios',
    '#options' => array('separate' => 'separate', 'collapse' => 'collapse'),
    '#title' => t('border-collapse'),
    '#default_value' => 'separate',
    '#attributes' => array('class' => 'table-options'),
  );
  $form['tabs']['table']['background-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[table][background-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['table']['background-color'] = array(
    '#type' => 'radios',
    '#options' => array('white' => 'white', '#FFFFF0' => '#FFFFF0', '#FAF0E6' => '#FAF0E6', '#FFF5EE' => '#FFF5EE', '#FFFAFA' => '#FFFAFA', 'custom' => drupal_render($form['tabs']['table']['background-color-custom'])),
    '#title' => t('background-color'),
    '#default_value' => 'white',
    '#attributes' => array('class' => 'table-options'),
  );
  $form['tabs']['thead']['border-width'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', 'thin' => 'thin', 'medium' => 'medium', 'thick' => 'thick', '0px' => '0px'),
    '#title' => t('border-width'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'thead-options'),
  );
  $form['tabs']['thead']['padding'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', '0px' => '0px'),
    '#title' => t('border-spacing'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'thead-options'),
  );
  $form['tabs']['thead']['border-style'] = array(
    '#type' => 'radios',
    '#options' => array('inset' => 'inset', 'none' => 'none', 'hidden' => 'hidden', 'dotted' => 'dotted', 'dashed' => 'dashed', 'solid' => 'solid', 'double' => 'double', 'ridge' => 'ridge', 'groove' => 'groove', 'outset' => 'outset'),
    '#title' => t('border-style'),
    '#default_value' => 'none',
    '#attributes' => array('class' => 'thead-options'),
  );
  $form['tabs']['thead']['border-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[thead][border-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['thead']['border-color'] = array(
    '#type' => 'radios',
    '#options' => array('white' => 'white', '#FFFFF0' => '#FFFFF0', '#FAF0E6' => '#FAF0E6', '#FFF5EE' => '#FFF5EE', '#FFFAFA' => '#FFFAFA', 'custom' => drupal_render($form['tabs']['thead']['border-color-custom'])),
    '#title' => t('border-color'),
    '#default_value' => 'white',
    '#attributes' => array('class' => 'thead-options'),
  );
  $form['tabs']['table']['-moz-border-radius'] = array(
    '#type' => 'radios',
    '#options' => array('0px' => '0px', '3px' => '3px', '6px' => '6px', '9px' => '9px', '12px' => '12px'),
    '#title' => t('-moz-border-radius'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'thead-options'),
  );
  $form['tabs']['th']['border-width'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', 'thin' => 'thin', 'medium' => 'medium', 'thick' => 'thick', '0px' => '0px'),
    '#title' => t('border-width'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['th']['padding'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', '0px' => '0px'),
    '#title' => t('border-spacing'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['th']['border-style'] = array(
    '#type' => 'radios',
    '#options' => array('inset' => 'inset', 'none' => 'none', 'hidden' => 'hidden', 'dotted' => 'dotted', 'dashed' => 'dashed', 'solid' => 'solid', 'double' => 'double', 'ridge' => 'ridge', 'groove' => 'groove', 'outset' => 'outset'),
    '#title' => t('border-style'),
    '#default_value' => 'none',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['th']['border-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[th][border-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['th']['border-color'] = array(
    '#type' => 'radios',
    '#options' => array('white' => 'white', '#FFFFF0' => '#FFFFF0', '#FAF0E6' => '#FAF0E6', '#FFF5EE' => '#FFF5EE', '#FFFAFA' => '#FFFAFA', 'custom' => drupal_render($form['tabs']['th']['border-color-custom'])),
    '#title' => t('border-color'),
    '#default_value' => 'white',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['th']['background-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[th][background-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['th']['background-color'] = array(
    '#type' => 'radios',
    '#options' => array('white' => 'white', '#FFFFF0' => '#FFFFF0', '#FAF0E6' => '#FAF0E6', '#FFF5EE' => '#FFF5EE', '#FFFAFA' => '#FFFAFA', 'custom' => drupal_render($form['tabs']['th']['background-color-custom'])),
    '#title' => t('background-color'),
    '#default_value' => 'white',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['th']['-moz-border-radius'] = array(
    '#type' => 'radios',
    '#options' => array('0px' => '0px', '3px' => '3px', '6px' => '6px', '9px' => '9px', '12px' => '12px'),
    '#title' => t('-moz-border-radius'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['tr']['border-width'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', 'thin' => 'thin', 'medium' => 'medium', 'thick' => 'thick', '0px' => '0px'),
    '#title' => t('border-width'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['tr']['padding'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', '0px' => '0px'),
    '#title' => t('border-spacing'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['tr']['border-style'] = array(
    '#type' => 'radios',
    '#options' => array('inset' => 'inset', 'none' => 'none', 'hidden' => 'hidden', 'dotted' => 'dotted', 'dashed' => 'dashed', 'solid' => 'solid', 'double' => 'double', 'ridge' => 'ridge', 'groove' => 'groove', 'outset' => 'outset'),
    '#title' => t('border-style'),
    '#default_value' => 'none',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['tr']['border-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[th][border-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['tr']['border-color'] = array(
    '#type' => 'radios',
    '#options' => array('white' => 'white', '#FFFFF0' => '#FFFFF0', '#FAF0E6' => '#FAF0E6', '#FFF5EE' => '#FFF5EE', '#FFFAFA' => '#FFFAFA', 'custom' => drupal_render($form['tabs']['tr']['border-color-custom'])),
    '#title' => t('border-color'),
    '#default_value' => 'white',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['tr']['background-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[th][background-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['tr']['background-color'] = array(
    '#type' => 'radios',
    '#options' => array('white' => 'white', '#FFFFF0' => '#FFFFF0', '#FAF0E6' => '#FAF0E6', '#FFF5EE' => '#FFF5EE', '#FFFAFA' => '#FFFAFA', 'custom' => drupal_render($form['tabs']['tr']['background-color-custom'])),
    '#title' => t('background-color'),
    '#default_value' => 'white',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['tr']['-moz-border-radius'] = array(
    '#type' => 'radios',
    '#options' => array('0px' => '0px', '3px' => '3px', '6px' => '6px', '9px' => '9px', '12px' => '12px'),
    '#title' => t('-moz-border-radius'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'th-options'),
  );
  $form['tabs']['td']['border-width'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', 'thin' => 'thin', 'medium' => 'medium', 'thick' => 'thick', '0px' => '0px'),
    '#title' => t('border-width'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'td-options'),
  );
  $form['tabs']['td']['padding'] = array(
    '#type' => 'radios',
    '#options' => array('1px' => '1px', '2px' => '2px', '3px' => '3px', '4px' => '4px', '5px' => '5px', '0px' => '0px'),
    '#title' => t('border-spacing'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'td-options'),
  );
  $form['tabs']['td']['border-style'] = array(
    '#type' => 'radios',
    '#options' => array('inset' => 'inset', 'none' => 'none', 'hidden' => 'hidden', 'dotted' => 'dotted', 'dashed' => 'dashed', 'solid' => 'solid', 'double' => 'double', 'ridge' => 'ridge', 'groove' => 'groove', 'outset' => 'outset'),
    '#title' => t('border-style'),
    '#default_value' => 'none',
    '#attributes' => array('class' => 'td-options'),
  );
  $form['tabs']['td']['border-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[td][border-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['td']['border-color'] = array(
    '#type' => 'radios',
    '#options' => array('white' => 'white', '#FFFFF0' => '#FFFFF0', '#FAF0E6' => '#FAF0E6', '#FFF5EE' => '#FFF5EE', '#FFFAFA' => '#FFFAFA', 'custom' => drupal_render($form['tabs']['td']['border-color-custom'])),
    '#title' => t('border-color'),
    '#default_value' => 'white',
    '#attributes' => array('class' => 'td-options'),
  );
  $form['tabs']['td']['background-color-custom'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#value' => '#ffffff',
    '#name' => 'tabs[td][background-color-custom]', // set our own name tag as we're using drupal_render
    '#attributes' => array('class' => 'color_textfield'), // identify so farbtastic can link to it
  );
  $form['tabs']['td']['background-color'] = array(
    '#type' => 'radios',
    '#options' => array('white' => 'white', '#FFFFF0' => '#FFFFF0', '#FAF0E6' => '#FAF0E6', '#FFF5EE' => '#FFF5EE', '#FFFAFA' => '#FFFAFA', 'custom' => drupal_render($form['tabs']['td']['background-color-custom'])),
    '#title' => t('background-color'),
    '#default_value' => 'white',
  );
  $form['tabs']['td']['-moz-border-radius'] = array(
    '#type' => 'radios',
    '#options' => array('0px' => '0px', '3px' => '3px', '6px' => '6px', '9px' => '9px', '12px' => '12px'),
    '#title' => t('-moz-border-radius'),
    '#default_value' => '0px',
    '#attributes' => array('class' => 'td-options'),
  );
  $form['preview'] = array('#type' => 'fieldset', '#title' => t('Preview:'));
  $form['preview']['table'] = array('#type' => 'item', '#value' => tablemanager_css_example_table());
  $form['actions'] = array('#prefix' => '<div class="container-inline">', '#suffix' => '</div>');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Generate'));
  $form['actions']['cancel'] = array('#value' => l(t('Cancel'), 'admin/content/tablemanager'));
  return $form;
} // tablemanager_css_styling

function tablemanager_css_styling_submit($form, &$form_state) {
  // Cycle through each tab
  foreach (element_children($form_state['values']['tabs']) as $element) {
    $output .= $element == 'table' ? "table#tablemanager-example-table {\n" : "table#tablemanager-example-table $element {\n";
    foreach (element_children($form_state['values']['tabs'][$element]) as $style) {
      $value = $form_state['values']['tabs'][$element][$style] == 'custom' ? $form_state['clicked_button']['#post']['tabs'][$element]["$style-custom"] : $form_state['values']['tabs'][$element][$style];
      $output .= "  $style: $value;\n";
    }
    $output .= "}\n";
  }
  print $output .'<p>To-do...  This isn\'t really all that high on my list of priorities at the moment.  If anyone wants to help - that\'d be fantastic.  I\'m not so good with javascript, jquery or even css - all this was just a little experiment...</p>';
} // tablemanager_css_styling_submit

function theme_tablemanager_css_styling($form) {
  // Add Farbtastic color picker
  drupal_add_css('misc/farbtastic/farbtastic.css');
  drupal_add_js('misc/farbtastic/farbtastic.js');
  // Add our own js and css
  drupal_add_css(drupal_get_path('module', 'tablemanager_css') .'/misc/tablemanager_css.css');
  drupal_add_js(drupal_get_path('module', 'tablemanager_css') .'/misc/tablemanager_css.js');

  $output = '<div style="text-align: right">'. drupal_render($form['styles']) .'</div>';
  $output .= "<div class=\"tabs\"><ul class=\"tabNavigation\">";
  foreach (element_children($form['tabs']) as $tab) {
    $output .= "<li><a href=\"#$tab\">$tab</a></li>";
    $build .= "<div id=\"$tab\">";
    foreach (element_children($form['tabs'][$tab]) as $element) {
      $build .= '<div class="container-inline">'. drupal_render($form['tabs'][$tab][$element]) .'</div>';
    }
    $build .= '</div>';
  }
  $output .= '</li></ul>'. $build .'</div><p /><div align="center" class="colorpicker"></div>';

  // Render remaining fields
  $output .= drupal_render($form);
  return $output;
} // theme_tablemanager_css_styling

function tablemanager_css_example_table() {
  $header = array(array('data' => t('Name')), array('data' => t('Age'), 'field' => '2', 'sort' => 'asc'), array('data' => t('IQ')));
  $rows[] = array(array('data' => 'Peter'), array('data' => '35'), array('data' => '95'));
  $rows[] = array(array('data' => 'Lois'), array('data' => '30'), array('data' => '115'));
  $rows[] = array(array('data' => 'Paul'), array('data' => '33'), array('data' => '125'));
  return theme('table', $header, $rows, array('id' => 'tablemanager-example-table'));
} // tablemanager_css_example_table
