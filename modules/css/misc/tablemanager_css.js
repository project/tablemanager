
// Global killswitch: only run if we are in a supported browser.
if (Drupal.jsEnabled) {
  $(document).ready(function () {
    var tabContainers = $('div.tabs > div');
    var farb = $.farbtastic("div.colorpicker");
    tabContainers.hide().filter(':first').show();

    // tabs;
    $('div.tabs ul.tabNavigation a').click(function () {
      tabContainers.hide();
      tabContainers.filter(this.hash).show();
      $('div.tabs ul.tabNavigation a').removeClass('selected');
      $(this).addClass('selected');
      return false;
    }).filter(':first').click();

    // colorpicker
    $("input.color_textfield").each(function () {
      farb.linkTo(this);
      $(this).click(function () {
        var field = this.name;
        farb.linkTo(function () {
          var name = tablemanager_convert(field);
          $("input[name='" + field + "']").css('backgroundColor', farb.color);
          $("input[name='" + field + "']").attr('value', farb.color);
          // only update preview table if the corresponding radio button is checked
          if ($("input[name='" + tablemanager_normal_field(field) + "']:checked").attr('value') == 'custom') {
            $(name[1]).css(tablemanager_normal_field(name[2]), farb.color);
          }
        });
      }).filter(':first').click();
    });

    // pre-defined styles selection
    $("select.table-styles").change(function () {
      alert("Haven't actually implemented this yet...");
      $(this).val(0);
    });

    // set preview table to style defaults (or on a screen refresh)
    $("input[type=radio]:checked").each(function () {
      // refresh values
      var name = tablemanager_convert(this.name);
      if (this.value == 'custom') {
        $(name[1]).css(name[2], $("input[name='" + tablemanager_custom_field(this.name) + "']").val());
      }
      else {
        $(name[1]).css(name[2], this.value);
      }
    });

    // tablemanager preview table alterations when options are clicked/ changed
    $("input[type=radio]").click(function () {
      var name = tablemanager_convert(this.name);
      if (this.value == 'custom') {
        $(name[1]).css(name[2], $("input[name='" + tablemanager_custom_field(this.name) + "']").val());
      }
      else {
        $(name[1]).css(name[2], this.value);
      }
    });

    // catch 'custom' textfield changes
    $("input[type=text]").keyup(function() {
      $("input[name='" + tablemanager_normal_field(this.name) + "']").click();
    });

    /**
     *  converts given name into an array containing the correct object selector and table style
     */
    function tablemanager_convert(name) {
      name = name.split(/(?:tabs)(?:\[)(.+)(?:\]\[)(.+)(?:\])/g);
      if (name[1] == 'table') {
        name[1] = 'table#tablemanager-example-table';
      }
      else {
        name[1] = 'table#tablemanager-example-table ' + name[1];
      }
      return name;
    }

    /**
     *  Returns the custom colour field for an option which uses farbtastic
     */
    function tablemanager_custom_field(field) {
      return field.replace(/\]$/, '-custom]');
    }

    /**
     *  Returns the 'normal' field when passed a custom field id
     */
    function tablemanager_normal_field(field) {
      return field.replace(/\-custom/, '');
    }

  });
}
