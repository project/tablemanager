<?php

/**
 * Implementation of hook_help().
 */
function tablemanager_csv_help($path, $arg) {
  switch ($path) {
    case 'admin/content/tablemanager/csv_import': // This description is shown on the CSV import screen
      return t("<p>Cut and paste your CSV file into the textbox - then click submit to import into 'Table Manager'.
                <br />You will be informed whether the import was successful or not.</p>
                <p><strong>Make sure you're not using a WYSIWYG editor here</strong>, as it'll insert &lt;p&gt;&lt;/p&gt; tags instead of using the required newlines.
                <br />Either use the form in 'plain text editor' mode, else specifically exclude the form in your WYSIWYG settings;
                <br />The element is: <strong>edit-csvfile</strong> - the path is: <strong>admin/content/tablemanager/csv_import</strong></p>");
  }
} // tablemanager_csv_help

/**
 * Implementation of hook_menu().
 */
function tablemanager_csv_menu() {
  $items = array();
  $items['admin/content/tablemanager/csv_import'] = array(
    'title' => 'Import CSV',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_csv_import'),
    'access arguments' => array('administer tables'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/content/tablemanager/csv_export'] = array(
    'title' => 'Export CSV',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tablemanager_csv_export'),
    'access arguments' => array('administer tables'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  return $items;
} // tablemanager_csv_menu

/**
 * Allows a user to import a CSV file.
 */
function tablemanager_csv_import(&$form_state) {
  $tables = tablemanager_load_tables();
  $tables[0] = t('New Table');
  ksort($tables);
  $form['table'] = array(
    '#type' => 'select',
    '#title' => t('Add CSV to table'),
    '#default_value' => isset($form_state['values']['table']) ? $form_state['values']['table'] : 0,
    '#options' => $tables,
  );
  $form['separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Separator'),
    '#default_value' => isset($form_state['values']['separator']) ? $form_state['values']['separator'] : ',',
    '#description' => t('The character which separates each entry<br />NOTE newlines always separate each row'),
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE,
  );
  $form['csvfile'] = array(
    '#type' => 'textarea',
    '#title' => t('Paste CSV Here'),
    '#description' => t('If one of your entries contains your separator (specified above) you can still include it by placing quotes around that entry, eg:
      <table>
        <tr>
          <td>Type,</td>
          <td>Description,</td>
          <td>Size,</td>
          <td>RRP,</td>
          <td>Sale Price</td>
        </tr>
        <tr>
          <td>New 2004,</td>
          <td>"Ambrosio Vortex, No Fork, Carbon Stays",</td>
          <td>53x53,</td>
          <td>RRP &pound;475,</td>
          <td>Now &pound;237+VAT</td>
        </tr>
      </table>'),
    '#cols' => 60,
    '#rows' => 25,
    '#default_value' => isset($form_state['values']['csvfile']) ? $form_state['values']['csvfile'] : '',
  );
  $form['upload'] = array(
    '#type' => 'fieldset',
    '#title' => t('-- Upload a CSV file --'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['upload']['csv_upload'] = array(
    '#type' => 'file',
    '#size' => 25,
    '#description' => t('Click "Browse..." to select a csv file to upload.'),
  );
  $form['upload']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );
  $form['header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use first line as HEADER?'),
    '#return_value' => 1,
    '#default_value' => isset($form_state['values']['header']) ? $form_state['values']['header'] : TRUE,
    '#description' => t('Check this box if the first line of your CSV is the header (like in the example given above)<br />You need to be creating a new table for this to take effect'),
  );
  $form['format'] = filter_form($form['format']);

  // Add the confirm_form before setting the 'enctype' form attribute (else it gets overwritten)
  $form = confirm_form($form, NULL, 'admin/content/tablemanager', ' ', t('Submit'), t('Cancel'));

  // Set form parameters so we can accept file uploads (and restore the class for confirm_form above).
  $form['#attributes'] += array('enctype' => 'multipart/form-data');

  return $form;
} // tablemanager_csv_import

/**
 * Validate function for tablemanager_csv_import form.
 */
function tablemanager_csv_import_validate($form, &$form_state) {
//  $form_state['storage'] = $form_state['values'];
  switch ($form_state['clicked_button']['#value']) {
    case t('Upload'):
      if (!$file = file_save_upload('csv_upload')) {
        form_set_error('csv_upload', t('File upload failed.'));
      }
      else {
        $form_state['rebuild'] = TRUE;
        form_set_value(array('#parents' => array('csvfile')), file_get_contents($file->filepath), $form_state);
        file_delete($file->filepath);
//        form_set_error('csvfile', t('Check your uploaded CSV and click \'Submit\''));
      }
      break;
    case t('Submit'):
      if (!$form_state['values']['csvfile']) {
        if (!$file = file_save_upload('csv_upload')) {
          form_set_error('csvfile', t('No CSV data supplied!'));
        }
        else {
          form_set_value(array('#parents' => array('csvfile')), file_get_contents($file->filepath), $form_state);
          file_delete($file->filepath);
        }
      }
      break;
  } // end switch
} // tablemanager_csv_import_validate

/**
 * Submit function for tablemanager_csv_import form.
 */
function tablemanager_csv_import_submit($form, &$form_state) {
  global $user;
  $tables = tablemanager_load_tables();
  if (!$form_state['values']['csvfile']) {
    drupal_set_message(t('Error creating CSV table.'));
    watchdog('tablemanager', 'tablemanager: error creating CSV table', WATCHDOG_ERROR);
    $form_state['redirect'] = 'admin/content/tablemanager';
    return;
  }
  $whole = trim($form_state['values']['csvfile']);
  $temp = explode("\n", $whole);
  foreach ($temp as $a) {
    if (!$header && $form_state['values']['table'] == '0') {
      if ($form_state['values']['header'] == 1) {
        $header = explode($form_state['values']['separator'], check_plain($a));
      }
      else {
        $count = count(explode($form_state['values']['separator'], $a));
        $header = array_fill(0, $count, NULL);
      }
      // insert header into database
      foreach ($header as $i) {
        $blah[] = array('data' => $i, 'type' => '1');
      }
      $header = serialize($blah);
      db_query("INSERT INTO {tablemanager} (uid, header) VALUES (%d, '%s')", $user->uid, $header);
      // update name to show tid
      $sql = db_fetch_object(db_query('SELECT tid FROM {tablemanager} ORDER BY tid DESC'));
      $tid = $sql->tid;
      db_query("UPDATE {tablemanager} SET name = '%s' WHERE tid = %d", $tid, $tid);
      if ($form_state['values']['header'] == 1) {
        continue;
      }
    }
    elseif (!$header) {
      $header = TRUE;
      $tid = $form_state['values']['table'];
    }
    $temp = check_markup($a, $form_state['values']['format']);
    if (substr(trim($temp), 0, 3) == "<p>" && trim(substr(trim($temp), -4, 4) == "</p>")) {
      $temp = substr(trim($temp), 3, -4);
    }
    // cope with quoted text
    $row = array();
    unset($quoted);
    $temp = explode($form_state['values']['separator'], $temp);
    foreach ($temp as $a) {
      if (substr($a, 0, 1) == '"' || $quoted) {
        $quoted .= $a . $form_state['values']['separator'];
        if (substr($a, -1, 1) == '"') {
          $row[] = substr($quoted, 1, -2);
          unset($quoted);
        }
      }
      else {
        $row[] = $a;
      }
    }
    $row = serialize($row);
    // insert rows into database
    db_query("INSERT INTO {tablemanager_data} (tid, uid, data) VALUES (%d, %d, '%s')", $tid, $user->uid, $row);
  } // end foreach
  variable_set('tablemanager_table', $tid);
  if ($form_state['values']['table'] == '0') {
    drupal_set_message(t('CSV table has been created successfully.'));
    watchdog('tablemanager', 'tablemanager: added table \'%table\' from CSV file.', array('%table' => $tid), WATCHDOG_NOTICE, l('view', "tables/$tid"));
  }
  else {
    drupal_set_message(t('CSV entries added to table \'%table\' successfully.', array('%table' => $tid)));
    watchdog('tablemanager', 'tablemanager: added entries to table \'%table\' from CSV file.', array('%table' => $tid), WATCHDOG_NOTICE, l('view', "tables/$tid"));
  }
  $form_state['redirect'] = 'admin/content/tablemanager';
} // tablemanager_csv_import_submit

/**
 * Allows a user to export a CSV file.
 */
function tablemanager_csv_export($form_state) {
  $tables = tablemanager_load_tables();
  if (!$tables) {
    return array('message' => array('#type' => 'item', '#value' => t('No tables defined yet.<p>!link</p>', array('!link' => l('Create Table', 'node/add/table')))));
  }
  $form['table'] = array(
    '#type' => 'select',
    '#title' => t('Table to export as CSV'),
    '#options' => $tables,
  );
  $form['separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Separator'),
    '#default_value' => ',',
    '#description' => t('The character to separate each entry<br />NOTE newlines always separate each row'),
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE,
  );
  return confirm_form($form, NULL, 'admin/content/tablemanager', ' ', t('Create'), t('Cancel'));
} // tablemanager_csv_export

/**
 * Submit function for tablemanager_csv_export form.
 */
function tablemanager_csv_export_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/content/tablemanager';
  $name = str_replace(' ', '_', tablemanager_fetch_name($form_state['values']['table']));
  $fetch = db_fetch_object(db_query('SELECT name, header FROM {tablemanager} WHERE tid = %d', $form_state['values']['table']));
  $name = $fetch->name == $form_state['values']['table'] ? "Table-$fetch->name" : str_replace(' ', '_', $fetch->name);
  $data = unserialize($fetch->header);
  foreach ($data as $build) {
    $csv_header[] = $build['data'];
  }
  $csv_header = implode($form_state['values']['separator'], $csv_header);
  $query = db_query('SELECT data FROM {tablemanager_data} WHERE tid = %d ORDER BY id', $form_state['values']['table']);
  while ($fetch = db_fetch_object($query)) {
    $csv_data[] = implode($form_state['values']['separator'], unserialize($fetch->data));
  }
  $csv = implode("\n", $csv_data);

  // need to 'Drupal-ize' - not sure how as this isn't actually a file?
  ob_end_clean();
  $headers[] = "Content-Disposition: attachment; filename = $name.csv";
  $headers[] = "Content-type: text/csv";
  foreach ($headers as $header) {
    $header = preg_replace('/\r?\n(?!\t| )/', '', $header);
    drupal_set_header($header);
  }
  print "$csv_header\n$csv";
  exit();
} // tablemanager_csv_export_submit
