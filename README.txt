---------
Overview:
---------

A simple module which automates the creation of tables and
allows you and permissioned users to add to them and maintain
them. The tables can be displayed in any node simply by typing:

[tablemanager:1]

The best thing about it is that the tables it creates are Drupal
themed tables and so easily fit in with the look and feel of
your site. It's also possible to use this module to display nice
looking themed bar charts.

Tablemanager supports pagination (splitting the results into pages
- NOTE the default is to display ALL the results). For instance,
to display table 2 limited to 25 results per page you'd type:

[tablemanager:2, 25]

If you write your own php snippets you can display your tables
from within your code:

<?php
 ...
  $output .= "This is table '4':<p>";
  $output .= tablemanager_display(4, 20);
  $output .= "</p>";
 ...
  return $output;
?>

If you are providing your users with certain administrative
privileges you can also display links to allow them to add new
rows and edit/ delete their own entries. This is set by either
a TRUE (add, edit and delete links ON) or FALSE (links OFF
which is the default).

[tablemanager:1, 35, TRUE]

If you wish to restrict the rows displayed you can do so by
passing arguments to the filter like this:

[tablemanager:1, NULL, FALSE, column = 2 | start = "a" | end = "h"]

If you wish to only show rows which match a particular string
you can omit the 'end' key:

[tablemanager:1, NULL, FALSE, column = 1 | start = "New 2006"]

For an advanced example; how to show everyone in a 'date
of birth' table who is under 25 years of age using the php
input filter:

<?php print tablemanager_display(1, NULL, FALSE, array('column' => 3, 'start' => date('Y/m/d', mktime(0, 0, 0, date('m'), date('d'), date('Y')-25)), 'end' => date('Y/m/d'))); ?>

You can also pass parameters to the filter for the <table>
tag, such as setting borders (and border rules), frame rules,
cellspacing/ cellpadding and alignment. Experiment with
these parameters on one of your own tables:

------------------------------------------------------------------------------------------------------
[tablemanager:1, NULL, FALSE, NULL, border = 2 | cellpadding = 5]
[tablemanager:1, NULL, FALSE, NULL, border = 5 | bgcolor = yellow]
[tablemanager:1, NULL, FALSE, NULL, border = 20 | frame = hsides]
[tablemanager:1, NULL, FALSE, NULL, title = My Table | bgcolor = yellow | border = 5 | frame = vsides]
------------------------------------------------------------------------------------------------------

For more information please visit this page:

http://www.w3schools.com/tags/tag_table.asp

Whilst its easy enough to create tables manually, this module
allows you to avoid writing any code at all (so great for
non-programmers) and inserts your tables into the database so
any changes you make to a table will reflect on whatever pages
you have the table displayed on - which can be however many
pages you like. You can create as many tables as you like and
manage all of them in the admin screen.

It's now also possible to automatically create bar charts with
this module. When you create a new table you're given the option
to create it as a bar chart. There are several settings you can
apply as well, such as the high and low values acceptable to each
bar chart you create, plus the colour of the bars and theming/
display options.

You can edit your tables however you like;

	o edit the header, table name and description
	o allow end user to sort only on specified columns
          (or have no sorting at all)
	o set a column to be default for sorting when table
	  is first displayed (also set default for which
	  direction to sort in)
	o add more rows
	o edit all your data
	o delete rows
	o add and remove columns

In admin/menu you can enable a menu item which will list all
tables when a permissioned user/ administrator goes to:

http://www.yoursite.com/tablemanager

The user can then click on the links to be taken to the table
they wish to view. If you're permissioning users to be able to
create their own tables, this is where they'll appear - you
should also enable 'Display Administrative Links' in
admin/settings/tablemanager under 'Table List Screen Settings'
so these users can then manage the tables they have created.

------
To do:
------

 o Add an 'invisible' setting so that a row isn't displayed in main display function but is still
     present when a row is added/ edited (useful for comments?  hiding old rows?)
 o Perhaps add an automatic column of 'Submitted by' (if wanted? maybe as a data type?)
 o Remove the requirement that a column *must* be sortable to be sorted - it's no longer necessary
     and a nice feature to sort on a default column and not have to allow it be changed by an end user

Pobster